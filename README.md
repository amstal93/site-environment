# site-environment

Use Docker to create an environment for building and publishing my website.

Tools in use:

* openssh-client
* rsync
* tar
* git
* Hugo
